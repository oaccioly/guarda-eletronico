var resultado = document.querySelector(".resultado")

function multaVelocidade(velocidade, velocidade_maxima) {
    var velocidade_ultrapassada = (velocidade - velocidade_maxima) * 100 / velocidade_maxima
    //console.log("Guarda de Transito Eletronico")
    //console.log(`Voce esta Transitando a ${velocidade}Km/H em uma via de Velocidade Maxima de ${velocidade_maxima}Km/H`)
    //console.log(`${velocidade_ultrapassada.toFixed(2)}%`)
    console.log(velocidade_ultrapassada)
    if (velocidade > velocidade_maxima) {
        if (velocidade_ultrapassada <= 20) {
            //console.log("Voce foi Multado no Artigo 218 do CTB.")
            //console.log("Transitar em Velocidade Superior a Maxima Permitida em ate 20%.")
            //console.log("Multa Media: R$ 130,16 e 4 Pontos.")

            resultado.innerHTML = "<p>Voce foi Multado no Artigo 218 do CTB.</p><p>Transitar em Velocidade Superior a Maxima Permitida em ate 20%.</p><p>Multa Media: R$ 130,16 e 4 Pontos.</p>"
        } else if (velocidade_ultrapassada > 20 & velocidade_ultrapassada <= 50){
            //console.log("Voce foi Multado no Artigo 218 do CTB.")
            //console.log("Transitar em Velocidade Superior a Maxima Permitida de 20% a 50%.")
            //console.log("Multa Grave: R$ 195,23 e 5 Pontos.")

            resultado.innerHTML = "<p>Voce foi Multado no Artigo 218 do CTB.</p><p>Transitar em Velocidade Superior a Maxima Permitida de 20% a 50%.</p><p>Multa Grave: R$ 195,23 e 5 Pontos.</p>"
        } else if (velocidade_ultrapassada > 50) {
            //console.log("Voce foi Multado no Artigo 218 do CTB.")
            //console.log("Transitar em Velocidade Superior a Maxima Permitida em mais de 50%.")
            //console.log("Multa Mandataria: Suspensao Imediata do Direito de Dirigir.")
            //console.log("Multa Gravissima: 3x R$ 293,47 = R$ 880,41 e 20 Pontos.")

            resultado.innerHTML = "<p>Voce foi Multado no Artigo 218 do CTB.</p><p>Transitar em Velocidade Superior a Maxima Permitida em mais de 50%.</p><p>Multa Mandataria: Suspensao Imediata do Direito de Dirigir.<p>Multa Gravissima: 3x R$ 293,47 = R$ 880,41 e 20 Pontos.</p>"
        }
    } else {
        console.log("Voce esta correto, siga com Atençao.")

        resultado.innerHTML = "Voce esta correto, siga com Atençao."
    }
}

var placa = document.querySelector(".velocidade-maxima")
var multas = ["30", "40", "50", "60", "70",
              "80", "90", "100", "110", "120"]

function atualizar(){
    var velocidade = Number(document.getElementById("velocidade").value)
    var velocidade_maxima = Number(document.getElementById("velocidade_maxima").value)
    
    // Velocimetro e Placa
    if (multas.includes(String(velocidade_maxima))) {
        if (velocidade > 1 & velocidade <= 220) {
            placa.innerHTML = `<strong>${velocidade_maxima}</strong>`
            velocimetro.series[0].points[0].update(velocidade)
            multaVelocidade(velocidade, velocidade_maxima)
        }
    }
}
