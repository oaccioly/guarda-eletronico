Highcharts.chart('container', {

    chart: {
        type: 'gauge',
        alignTicks: false,
        plotBackgroundColor: null,
        plotBackgroundImage: null,
        plotBorderWidth: 0,
        plotShadow: false
    },

    title: {
        text: ''
    },

    pane: {
        startAngle: -150,
        endAngle: 150
    },

    yAxis: [{
        min: 0,
        max: 220,
        tickPosition: 'outside',
        lineColor: '#933',
        lineWidth: 2,
        minorTickPosition: 'outside',
        tickColor: '#933',
        minorTickColor: '#933',
        tickLength: 5,
        minorTickLength: 5,
        labels: {
            distance: 12,
            rotation: 'auto'
        },
        offset: -20,
        endOnTick: false
    }],

    series: [{
        name: 'Speed',
        data: [80],
        dataLabels: {
            formatter: function () {
                var kmh = this.y,
                    mph = Math.round(kmh * 0.621);
                return '<span style="color:#339">' + kmh + ' km/h</span><br/>'
            },
            backgroundColor: {
                linearGradient: {
                    x1: 0,
                    y1: 0,
                    x2: 0,
                    y2: 1
                },
                stops: [
                    [0, '#DDD'],
                    [1, '#FFF']
                ]
            }
        },
        tooltip: {
            valueSuffix: ' km/h'
        }
    }]

},
function(chart){});

var velocimetro = Highcharts.charts[0]
velocimetro.chartBackground.element.remove()


